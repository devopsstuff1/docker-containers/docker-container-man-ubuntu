FROM ubuntu:22.04

RUN sed -i 's:^path-exclude=/usr/share/man:#path-exclude=/usr/share/man:' /etc/dpkg/dpkg.cfg.d/excludes

RUN apt-get update && \
    apt-get install -y \
        man \
        manpages-posix \
				less

RUN yes | unminimize
